<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <title>Ejercicio 2-Formulario</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="estilos.css">
</head>

<body>
    <?php
    $_SESSION["voto"]=0;
    ?>
   
    <h2>REGISTRO NUEVA PELÍCULA</h2></br></br>
    <form action="ejercicio2.php" method="post">
        Título:<input type="text" required="required" name="titulo"></br></br>
        Año:<input type="number" required="required" name="anio"></br></br>
        Director:<input type="text" required="required" name="director"></br></br>
        Poster:<input type="text" required="required" name="poster"></br></br>
        Alquilada:<input type="number" min="0" max="1" required="required" name="alquilada"></br></br>
        Sinopsis:<input type="text" required="required" name="sinopsis"></br></br>
        Puntuación: 
        <?php /*if(isset($_POST["nota"]) && count($_POST["nota"])<=10 && count($_POST["nota"])>=0){
        for($i=0;$i<=count($_POST["nota"]);$i++){
      echo "<img heigth='20' width='20' src='estrella.jpg'>";
    }
}*/?><input type="number" required="required" min="0" max="10"
        name=nota></br></br>
        <input type="submit" name="enviar">
    </form>

</body>

</html>