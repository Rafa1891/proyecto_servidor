<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <title>Listado por año</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" href="estilos.css">
</head>
<body>
<?php
session_start();
 $host='localhost';
  $usuario='root';
  $password='';
  $nombre_bd='video';

  $conexion=mysqli_connect($host,$usuario,$password,$nombre_bd);

 if (mysqli_connect_errno()) {
printf("Conexión fallida: %s\n", mysqli_connect_error());
exit();
}

    ?>
    <table>
    	<tr>
    		<th>Película más nueva</th>
    		<th>Película más antigua</th>
    	</tr>
    	
	<?php
	$consulta="SELECT title FROM peliculas WHERE year=(SELECT MAX(year) FROM peliculas)";
    $resultado=mysqli_query($conexion,$consulta);
    $filas=mysqli_num_rows($resultado);

    $consulta2="SELECT title FROM peliculas WHERE year=(SELECT MIN(year) FROM peliculas)";
    $resultado2=mysqli_query($conexion,$consulta2);
    $filas2=mysqli_num_rows($resultado2);
    
    if($filas==0 ||$filas2==0){
      echo "No hay películas disponibles.";   
    }else{
	while($registro=mysqli_fetch_assoc($resultado)) {
		echo "<tr>";
        echo "<td>".$registro["title"]."</td>";
    }
    while($registro2=mysqli_fetch_assoc($resultado2)) {	
        echo "<td>".$registro2["title"]."</td>";
		echo "</tr>";
    }
}
	?>

</table>
    <table>
    	<tr>
    		<th>ID</th>
    		<th>Título</th>
    		<th>Año</th>
    		<th>Director</th>
    		<th>Póster</th>
    		<th>Sinopsis</th>
    		<th>Nota</th>
    	</tr>
    	
	<?php
	$consulta="SELECT * FROM peliculas ORDER BY year ASC";
    $resultado=mysqli_query($conexion,$consulta);
    $filas=mysqli_num_rows($resultado); 
    
    if($filas==0){
        echo "No hay películas disponibles.";   
      }else{

	while($registro=mysqli_fetch_assoc($resultado)) {
		echo "<tr>";
        echo "<td>".$registro["id"]."</td><td>".$registro["title"]."</td><td>".$registro["year"]
        ."</td><td><svg width=".($registro["year"]-1900)." height='30'>
        <line x1='0' y1='30' x2=".$registro["year"]." y2='30' stroke='blue' stroke-width='30' /> 
      </svg></td><td>".$registro["director"]."</td><td>".$registro["poster"]."</td><td>"
        .$registro["synopsis"]."</td><td>".$registro["nota"]."</td>";
		echo "</tr>";
	}
	?>

</table>
<?php
}
?>
</body>
</html>