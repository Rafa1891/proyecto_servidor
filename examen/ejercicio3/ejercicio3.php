<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <title>Listado por nota</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" href="estilos.css">
</head>
<body>
<?php
session_start();
 $host='localhost';
  $usuario='root';
  $password='';
  $nombre_bd='video';

  $conexion=mysqli_connect($host,$usuario,$password,$nombre_bd);

 if (mysqli_connect_errno()) {
printf("Conexión fallida: %s\n", mysqli_connect_error());
exit();
}

$consulta="SELECT * FROM peliculas WHERE rented=0 ORDER BY nota DESC";
$resultado=mysqli_query($conexion,$consulta);
$filas=mysqli_num_rows($resultado);


if($filas==0){
  echo "No hay películas disponibles.";   
}else{
    ?>
    <table>
    	<tr>
    		<th>ID</th>
    		<th>Título</th>
    		<th>Año</th>
    		<th>Director</th>
    		<th>Póster</th>
    		<th>Sinopsis</th>
    		<th>Nota</th>
    	</tr>
    	
	<?php
	
	while($registro=mysqli_fetch_assoc($resultado)) {
		echo "<tr>";
        echo "<td>".$registro["id"]."</td><td>".$registro["title"]."</td><td>".$registro["year"]
        ."</td><td>".$registro["director"]."</td><td>".$registro["poster"]."</td><td>"
        .$registro["synopsis"]."</td><td>".$registro["nota"]."</td><td><svg width=".$registro["nota"]." height='30'>
        <line x1='0' y1='30' x2=".$registro["nota"]." y2='30' stroke='blue' stroke-width='30' /> 
      </svg></td>";
		echo "</tr>";
	}
	?>

</table>
<?php
}
?>
</body>
</html>