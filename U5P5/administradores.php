<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <title>Administradores</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" href="estilos.css">
</head>
<body>
	<div>
<?php
session_start();
 $host='localhost';
  $_SESSION["usuario"]='administrador1';
  $password='';
  $nombre_bd='consultas';

  $conexion=mysqli_connect($host,$_SESSION["usuario"],$password,$nombre_bd);

 if (mysqli_connect_errno()) {
printf("Conexión fallida: %s\n", mysqli_connect_error());
exit();
}

if(isset($_POST["altapaciente"])){
  ?>
  <h2>Datos del nuevo paciente</h2></br></br>
  <form action="nuevopaciente.php" method="post">
  DNI:<input type="text" name="dni"></br></br>
  Nombre:<input type="text" name="nombre"></br></br>
  Apellidos:<input type="text" name="apellidos"></br></br>
  Fecha:<input type="date" name="fecha"></br></br>
  Sexo:<input type="text" name="sexo"></br></br>
  <input type="submit" name="enviar">
  </form>
  <?php

}

if(isset($_POST["altamedico"])){
  ?>
<h2>Datos del nuevo médico</h2></br></br>
  <form action="nuevomedico.php" method="post">
  DNI:<input type="text" name="dni"></br></br>
  Nombre:<input type="text" name="nombre"></br></br>
   Usuario:<input type="text" name="log"></br></br>
  Contraseña:<input type="password" name="pass"></br></br>
  Apellidos:<input type="text" name="apellidos"></br></br>
  Especialidad:<input type="text" name="especialidad"></br></br>
  Teléfono:<input type="text" name="telefono"></br></br>
  Correo:<input type="text" name="correo"></br></br>
  <input type="submit" name="enviar">
  </form>
  <?php

}

if(isset($_POST["cerrar"])){
  session_destroy();
  mysqli_close($conexion);
  header("Location:login.html");
}
?>
</div>
</body>
</html>