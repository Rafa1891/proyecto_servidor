<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <title>Asistentes</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" href="estilos.css">
</head>
<body>
	<div>
<?php
session_start();
 $host='localhost';
  $_SESSION["usuario"]='asistente';
  $password='';
  $nombre_bd='consultas';

  $conexion=mysqli_connect($host,$_SESSION["usuario"],$password,$nombre_bd);

 if (mysqli_connect_errno()) {
printf("Conexión fallida: %s\n", mysqli_connect_error());
exit();
}

if(isset($_POST["atendidas"])){

$consulta="SELECT idCita,citFecha,citHora,citPaciente,citMedico,citConsultorio,citEstado,citObservaciones FROM citas WHERE citEstado='Atendido'";
$resultado=mysqli_query($conexion,$consulta);
$filas=mysqli_num_rows($resultado);
$registro=mysqli_fetch_assoc($resultado);

if($filas==0){
  echo "No hay citas atendidas.";  
}else{
 ?>
    <table>
  <tr>
        <th>ID Cita</th>
        <th>Fecha</th>
        <th>Hora</th>
        <th>Paciente</th>
        <th>Medico</th>
        <th>Consultorio</th>
        <th>Estado</th>        
      </tr>
      
  <?php
  
  while($registro=mysqli_fetch_array($resultado,MYSQLI_ASSOC)) {
    
    echo "<tr>";
    echo "<td>".$registro["idCita"]."</td><td>".$registro["citFecha"]."</td><td>".$registro["citHora"]."</td><td>".$registro["citPaciente"]."</td><td>".$registro["citMedico"]."</td><td>".$registro["citConsultorio"]."</td><td>".$registro["citEstado"]."</td>";
    echo "</tr>";
  }
  ?>
</table>
<?php
}
}
if(isset($_POST["nuevacita"])){

  ?>
  <h2>Datos de la nueva cita</h2></br></br>
  <form action="nuevacita.php" method="post">
  ID Cita:<input type="number" name="idcita"></br></br>
  Fecha:<input type="date" name="fecha"></br></br>
  Hora:<input type="time" name="hora"></br></br>
  DNI paciente:<input type="text" name="dnip"></br></br>
  DNI medico:<input type="text" name="dnim"></br></br>
  Consultorio:<input type="text" name="consultorio"></br></br>
  Observaciones:<input type="text" name="observaciones"></br></br>
  <input type="submit" name="enviar">
  </form>
  <?php
  

}

if(isset($_POST["altapaciente"])){
  ?>
  <h2>Datos del nuevo paciente</h2></br></br>
  <form action="nuevopaciente.php" method="post">
  DNI:<input type="text" name="dni"></br></br>
  Nombre:<input type="text" name="nombre"></br></br>
  Usuario:<input type="text" name="log"></br></br>
  Contraseña:<input type="password" name="pass"></br></br>
  Apellidos:<input type="text" name="apellidos"></br></br>
  Fecha:<input type="date" name="fecha"></br></br>
  Sexo:<input type="text" name="sexo"></br></br>
  <input type="submit" name="enviar">
  </form>
  <?php
}

if(isset($_POST["pacientes"])){
 $consulta="SELECT dniPac,pacNombres,pacApellidos,pacFechaNacimiento,pacSexo FROM pacientes";
$resultado=mysqli_query($conexion,$consulta);
$filas=mysqli_num_rows($resultado);


if($filas==0){
  echo "No hay pacientes.";  
}else{
 ?>
  <table>
     <tr>
        <th>DNI</th>
        <th>Nombre</th>
        <th>Apellidos</th>
        <th>Fecha de nacimiento</th>
        <th>Género</th>
      </tr>
      
  <?php
  
  while($registro=mysqli_fetch_array($resultado,MYSQLI_ASSOC)) {
    
    echo "<tr>";
    echo "<td>".$registro["dniPac"]."</td><td>".$registro["pacNombres"]."</td><td>".$registro["pacApellidos"]."</td><td>".$registro["pacFechaNacimiento"]."</td><td>".$registro["pacSexo"]."</td>";
    echo "</tr>";
  }
  ?>
  </table>
<?php
} 
}


if(isset($_POST["cerrar"])){
  session_destroy();
  mysqli_close($conexion);
  header("Location:login.html");
}
?>
</div>
</body>
</html>