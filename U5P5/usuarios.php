<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <title>Médicos</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" href="estilos.css">
</head>
<body>
  
<?php
if(isset($_POST["enviar"])){
  session_start();
      $_SESSION["login"]=$_POST["login"];
      $_SESSION["clave"]=$_POST["clave"];

  $host='localhost';
  $usuario='administrador1';
  $password='';
  $nombre_bd='consultas';

  $conexion=mysqli_connect($host,$usuario,$password,$nombre_bd);

 if (mysqli_connect_errno()) {
printf("Conexión fallida: %s\n", mysqli_connect_error());
exit();
}

$consulta="SELECT usuLogin,usuPassword,usutipo FROM usuarios WHERE usuLogin='$_SESSION[login]' AND usuPassword='$_SESSION[clave]'";
$resultado=mysqli_query($conexion,$consulta);
$filas=mysqli_num_rows($resultado);
$registro=mysqli_fetch_assoc($resultado);

if($filas==0){
	echo "El usuario introducido no existe, valídese de nuevo.";
	 
}else if($registro["usutipo"]=="Administrador"){
  
echo "<strong>".$registro["usutipo"]."</strong><br/>";
?>
<div>
<form method="post" action="administradores.php">
<button name="altapaciente" type="submit" >Alta paciente</button><br/><br/>
<button name="altamedico" type="submit" >Alta médico</button><br/><br/>
<button name="cerrar" type="submit" >Cerrar sesión</button>
</form>
</div>
<?php

}else if($registro["usutipo"]=="Medico"){
  
  echo "<strong>".$registro["usutipo"]."</strong><br/>";
  ?>
  <div>
<form method="post" action="medicos.php">
  <button name="atendidas" type="submit" >Ver citas atendidas</button><br/><br/>
<button name="pendientes" type="submit" >Ver citas pendientes</button><br/><br/>
<button name="pacientes" type="submit" >Ver pacientes</button><br/><br/>
  <button name="cerrar" type="submit" >Cerrar sesión</button>
</form>
</div>
  <?php

}else if($registro["usutipo"]=="Asistente"){
  
  echo "<strong>".$registro["usutipo"]."</strong><br/>";
  ?>
  <div>
  <form method="post" action="asistentes.php">
    <button name="nuevacita" type="submit" >Nueva cita</button><br/><br/>
    <button name="atendidas" type="submit" >Ver citas atendidas</button><br/><br/>
    <button name="altapaciente" type="submit" >Alta paciente</button><br/><br/>
    <button name="pacientes" type="submit" >Ver pacientes</button><br/><br/>
  <button name="cerrar" type="submit" >Cerrar sesión</button>
  </form>
</div>
  <?php

}else if($registro["usutipo"]=="Paciente"){
  
  echo "<strong>".$registro["usutipo"]."</strong><br/>";
  ?>
  <div>
  <form method="post" action="pacientes.php">
   <button name="atendidas" type="submit" >Ver citas atendidas</button><br/><br/>
  <button name="cerrar" type="submit" >Cerrar sesión</button>
  </form>
</div>
  <?php
}

}else{
  header("Location:login.html");
}

	?>
  
</body>
</html>