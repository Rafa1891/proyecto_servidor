<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <title>Médicos</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" href="estilos.css">
</head>
<body>
	<div>
<?php
session_start();
 $host='localhost';
  $usuario='medico';
  $password='';
  $nombre_bd='consultas';
  
  $conexion=mysqli_connect($host,$usuario,$password,$nombre_bd);
  
 if (mysqli_connect_errno()) {
printf("Conexión fallida: %s\n", mysqli_connect_error());
exit();
}

$consulta="SELECT dniUsu FROM usuarios WHERE usuLogin='$_SESSION[login]'";
$resultado=mysqli_query($conexion,$consulta);
$filas=mysqli_num_rows($resultado);
$registro=mysqli_fetch_assoc($resultado);

if($filas==0){
	echo "No existe ese usuario.";	 
}else{
 $_SESSION["dni"]=$registro["dniUsu"];
}


if(isset($_POST["atendidas"])){

$consulta="SELECT idCita,citFecha,citHora,citPaciente,citMedico,citConsultorio,citEstado,citObservaciones FROM citas WHERE citEstado='Atendido' AND citMedico='$_SESSION[dni]'";
$resultado=mysqli_query($conexion,$consulta);
$filas=mysqli_num_rows($resultado);
$registro=mysqli_fetch_assoc($resultado);

if($filas==0){
	echo "No hay citas atendidas.";	 
}else{
 ?>
    <table>
	<?php
	
	foreach ($registro as $key => $resultado) {
		echo "<tr><th>".$key."</th></tr>";
		echo "<tr><td>".$registro[$key]."</td></tr>";
	}
	?>
</table>
<?php
}

}if(isset($_POST["pendientes"])){
	
  $consulta="SELECT idCita,citFecha,citHora,citPaciente,citMedico,citConsultorio,citEstado FROM citas WHERE citEstado='Asignado' AND citMedico='$_SESSION[dni]'";
$resultado=mysqli_query($conexion,$consulta);
$filas=mysqli_num_rows($resultado);
$registro=mysqli_fetch_array($resultado,MYSQLI_ASSOC);

if($filas==0){
	echo "No hay citas pendientes.";	 
}else{
	?>
    <table>
    	<tr>
    		<th>ID Cita</th>
    		<th>Fecha</th>
    		<th>Hora</th>
    		<th>Paciente</th>
    		<th>Medico</th>
    		<th>Consultorio</th>
    		<th>Estado</th>
    		<th>Atender cita</th>
    	</tr>
    	<form method='post' action='atendercitaform.html'>
	<?php
	
	while($registro) {
		echo "<tr>";
		echo "<td>".$registro["idCita"]."</td><td>".$registro["citFecha"]."</td><td>".$registro["citHora"]."</td><td>".$registro["citPaciente"]."</td><td>".$registro["citMedico"]."</td><td>".$registro["citConsultorio"]."</td><td>".$registro["citEstado"]."</td><td><button type='submit'>Atender</button></td>";
		echo "</tr>";
	}
	?>
</form>
</table>
<?php
 
}

/*if(isset($_POST["atenderCita"])){
 $consulta="UPDATE citas SET citEstado='Atendido',citObservaciones='$_POST[observaciones]',atenderCita=true WHERE citEstado='Asignado' AND citMedico='$_SESSION[dni]'";

if (mysqli_query($conexion, $consulta)) {
echo "Cita atendida con éxito.";
} else {
echo "Error : " . mysqli_error($conn);
}

}*/

}if(isset($_POST["pacientes"])){
 $consulta="SELECT dniPac,pacNombres,pacApellidos,pacFechaNacimiento,pacSexo FROM pacientes
 INNER JOIN citas ON pacientes.dniPac=citas.citPaciente 
 WHERE citMedico='$_SESSION[dni]'";
$resultado=mysqli_query($conexion,$consulta);
$filas=mysqli_num_rows($resultado);
$registro=mysqli_fetch_assoc($resultado);

if($filas==0){
	echo "No hay pacientes.";	 
}else{
 ?>
    <table>
	<?php
	
	foreach ($registro as $key => $resultado) {
		echo "<tr><th>".$key."</th></tr>";
		echo "<tr><td>".$registro[$key]."</td></tr>";
	}
	?>
</table>
<?php
} 
}

if(isset($_POST["cerrar"])){
  session_destroy();
  mysqli_close($conexion);
  header("Location:login.html");
}
?>
</div>
</body>
</html>