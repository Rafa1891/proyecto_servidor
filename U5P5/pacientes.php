<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <title>Pacientes</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" href="estilos.css">
</head>
<body>
	<div>
<?php
session_start();
 $host='localhost';
  $_SESSION["usuario"]='paciente';
  $password='';
  $nombre_bd='consultas';

  $conexion=mysqli_connect($host,$_SESSION["usuario"],$password,$nombre_bd);

 if (mysqli_connect_errno()) {
printf("Conexión fallida: %s\n", mysqli_connect_error());
exit();
}

$consulta="SELECT dniUsu FROM usuarios WHERE usuLogin='$_SESSION[login]'";
$resultado=mysqli_query($conexion,$consulta);
$filas=mysqli_num_rows($resultado);
$registro=mysqli_fetch_assoc($resultado);

if($filas==0){
  echo "No existe ese usuario.";   
}else{
 $_SESSION["dni"]=$registro["dniUsu"];
}

if(isset($_POST["atendidas"])){

$consulta="SELECT idCita,citFecha,citHora,citPaciente,citMedico,citConsultorio,citEstado,citObservaciones FROM citas WHERE citEstado='Atendido' AND citPaciente='$_SESSION[dni]'";
$resultado=mysqli_query($conexion,$consulta);
$filas=mysqli_num_rows($resultado);


if($filas==0){
  echo "No hay citas atendidas.";  
}else{
 ?>
    <table>
  <tr>
        <th>ID Cita</th>
        <th>Fecha</th>
        <th>Hora</th>
        <th>Paciente</th>
        <th>Medico</th>
        <th>Consultorio</th>
        <th>Estado</th>
      </tr>
      
  <?php
  
  while($registro=mysqli_fetch_array($resultado,MYSQLI_ASSOC)) {
    
    echo "<tr>";
    echo "<td>".$registro["idCita"]."</td><td>".$registro["citFecha"]."</td><td>".$registro["citHora"]."</td><td>".$registro["citPaciente"]."</td><td>".$registro["citMedico"]."</td><td>".$registro["citConsultorio"]."</td><td>".$registro["citEstado"]."</td>";
    echo "</tr>";
  }
  ?>

</table>
<?php
}

}

if(isset($_POST["cerrar"])){
  session_destroy();
  mysqli_close($conexion);
  header("Location:login.html");
}
?>
</div>
</body>
</html>