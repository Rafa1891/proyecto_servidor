<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <title>Ejercicio 18</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <style>
  	table{
  		border:1px solid black;
  	}
  </style>
</head>
<body>  
  <?php 
$lenguajes_cliente["uno"]="Javascript";
$lenguajes_cliente["dos"]="HTML";
$lenguajes_cliente["tres"]="CSS";
$lenguajes_servidor["a"]="PHP";
$lenguajes_servidor["b"]="SQL";
$lenguajes_servidor["c"]=".NET";

$lenguajes=array_merge($lenguajes_cliente,$lenguajes_servidor);
?>
 <table>
 	<?php
foreach ($lenguajes as $key=>$v ) {	
	?>
	<tr>
		<th><?php echo $key ?></th>
    </tr>
	<tr>
		<td><?php echo $v ?></td>
    </tr>
<?php	
}
?>
 </table>  
</body>
</html>