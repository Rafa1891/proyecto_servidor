<?php
session_start();
  $conexion=mysqli_connect($_SESSION["host"],$_SESSION["usuario"],$_SESSION["password"],$_SESSION["nombrebd"]);

 if (mysqli_connect_errno()) {
printf("Conexión fallida: %s\n", mysqli_connect_error());
exit();
}

if(isset($_POST["visualizar"])){
  header("Location:productos.php");
}

if(isset($_POST["cerrar"])){
  session_destroy();
  mysqli_close($conexion);
  header("Location:formulario.html");
}
?>