<?php

  $conexion=mysqli_connect($_SESSION["host"],$_SESSION["usuario"],$_SESSION["password"],$_SESSION["nombrebd"]);

 if (mysqli_connect_errno()) {
printf("Conexión fallida: %s\n", mysqli_connect_error());
exit();
}
if(isset($_POST["introducir"])){
	?>
	<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <title>Nuevo artículo</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" href="estilos.css">
</head>
<body>
  <div>
	<h1>Introduzca un nuevo artículo:</h1><br/><br/>
	<form method="post" action="nuevoarticulo.php">
   Descripción:<input type="text" required="required" name="descripcion"><br/><br/>
   Precio:<input type="number" required="required" name="precio"><br/><br/>
   Características:<input type="text" required="required" name="caracteristicas"><br/><br/>
   Ruta de la imagen (URL):<input type="text" required="required" name="imagen"><br/><br/>
  <button name="enviar" type="submit" >Enviar</button></br>

</form>
</div>
</body>
</html>
<?php
}

if(isset($_POST["visualizar"])){
  header("Location:productos.php");
}

if(isset($_POST["cerrar"])){
  session_destroy();
  mysqli_close($conexion);
  header("Location:formulario.html");
}
?>