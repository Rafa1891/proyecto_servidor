<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <title>Productos</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" href="estilos.css">
</head>
<body>
  <?php

    session_start();

    $conexion=mysqli_connect($_SESSION["host"],$_SESSION["usuario"],$_SESSION["password"],$_SESSION["nombrebd"]);
      
      if (mysqli_connect_errno()) {
printf("Conexión fallida: %s\n", mysqli_connect_error());
exit();
}
                      
    ?>
    <div><h3>Bienvenid@ <?php echo $_SESSION["login"];?></h3></div><br/><br/>
    <form action="" method="post">
    <?php
     
$consulta="SELECT id_articulo,descripcion,precio,caracteristicas,imagen FROM articulos";
$resultado=mysqli_query($conexion,$consulta);
$filas=mysqli_num_rows($resultado);

$suma=0;

    while($registro=mysqli_fetch_array($resultado,MYSQLI_ASSOC)){
      echo "<div>";
      ?>
      <img src="<?php echo $registro['imagen']?>" alt="<?php echo $registro['descripcion']?>" width="150" height="150">
       <?php
      echo "<ul>
      <li>ID:<strong>".$registro["id_articulo"]."</strong></li>
      <li>Descripción:<strong>".$registro["descripcion"]."</strong></li>
      <li>Precio:<strong>".$registro["precio"]."</strong></li>
      <li>Características:<strong>".$registro["caracteristicas"]."</strong></li>
    </ul><br/>";
    
    ?>
    <button name="aniadir" value="<?php echo $suma;$suma=$suma+1;?>"  type="submit">Añadir al carrito</button></br></br>
     <button name="eliminar" value="<?php echo $suma;$suma=$suma-1;?>"  type="submit">Eliminar</button>
    </div>
    <?php
    }
    ?>
  </form>

  <div>
  <h3>Su compra:</h3>
  <br/>
  
  <?php
  $_SESSION['carrito']=$registro;
   
   for($i=0;$i<$filas;$i++){
    $cantidades[$i]=0;
   }

$_SESSION['cantidad']=$cantidades;

   if(isset($_POST["aniadir"])){ 
    
    $_SESSION['cantidad'][$_POST["aniadir"]]+=1;

    
    }
    if(isset($_POST["eliminar"])){
      if($_SESSION['cantidad'][$_POST["eliminar"]]>=1){
    $_SESSION['cantidad'][$_POST["eliminar"]]-=1;
  }

  }
  if(isset($_POST["aniadir"]) || isset($_POST["eliminar"])){
    
  $total=$_SESSION['carrito']['precio']*$_SESSION['cantidad'][$_POST["aniadir"]];
  $_SESSION['total']=$total;
  }
    //var_dump($_SESSION["cantidad"]);

    for($i=0;$i<$filas;$i++){
    echo $_SESSION['cantidad'][$_POST["aniadir"]]."  ".$_SESSION['carrito']['descripcion']." ".$_SESSION['carrito']['precio']." € .";
    }
    ?>
  </form>

<br/>
<form method="post" action="confirmar.php">
<button name="confirmar" id="botonconfirmar">Confirmar compra</button>
</form>
</div>
</body>
</html>
