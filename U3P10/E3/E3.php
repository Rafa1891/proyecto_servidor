<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <title>Ejercicio 3</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <style type="text/css">
 body{
  margin-left:200px;
 }
 svg{
  border:2px solid black;
 }
 div{
  float:left;
  border:1px solid blue;
  margin-right: 20px;
  padding: 5px;
 }
 #botonarriba{
   margin-left: 80px;
 }
 #botonabajo{
   margin-left: 80px;
 }
 </style>
</head>
<body>
  <h1>MOVER UN PUNTO EN DOS DIMENSIONES</h1>
  <p>Haga clic en los botones para mover el punto:</p><br/>
  <form method="post" action="E3b.php">
  <?php 
  
  session_start();

     if(!isset($_SESSION["x"]) || !isset($_SESSION["y"])){
    $_SESSION["x"]=$_SESSION["y"]=0;
}
   ?>
   <div>
  <button id="botonarriba" name="boton" type="submit" value="arriba">Arriba</button><br/><br/>
	<button name="boton" type="submit" value="izda">Izquierda</button>&nbsp
  <button type="submit" name="boton" value="centro">Centro</button>&nbsp
  <button type="submit" name="boton" value="dcha">Derecha</button><br/><br/>
  <button id="botonabajo" type="submit" name="boton" value="abajo">Abajo</button>
</div>
  <td>
  <svg width="400" height="400" viewbox="-200 -200 400 400">
  <circle cx="<?php echo $_SESSION['x']; ?>" cy="<?php echo $_SESSION['y']; ?>" r="8" fill="red" />  
</svg>
</td><br/>

</form>
</body>
</html>