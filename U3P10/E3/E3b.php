<?php
    
    session_start();

    if(!isset($_SESSION["x"]) || !isset($_SESSION["y"])){
    $_SESSION["x"]=$_SESSION["y"]=0;
}

function recoge($var){
 $tmp =(isset($_REQUEST[$var]))
 ? trim(htmlspecialchars($_REQUEST[$var],ENT_QUOTES,"UTF-8")):"";
 return $tmp;
}
$accion=recoge("boton");
$accionOk=false;

if($accion !="izda" && $accion !="dcha" && $accion !="centro" && $accion !="arriba" && $accion !="abajo"){
  header("Location:E3.php");
  exit;
}else{
  $accionOk=true;
}

if($accionOk){
  if($accion=="centro"){
    $_SESSION["x"]=$_SESSION["y"]=0;
  }else if($accion =="izda"){
    $_SESSION["x"]-=20;
  }else if($accion =="dcha"){
    $_SESSION["x"]+=20;
  }else if($accion =="arriba"){
    $_SESSION["y"]-=20;
  }else if($accion =="abajo"){
    $_SESSION["y"]+=20;
  }

  if($_SESSION["x"]>200){
    $_SESSION["x"]=-200;
  }else if($_SESSION["x"]<-200){
    $_SESSION["x"]=200;
  }

  if($_SESSION["y"]>200){
    $_SESSION["y"]=-200;
  }else if($_SESSION["y"]<-200){
    $_SESSION["y"]=200;
  }
  header("Location:E3.php");
  exit;
}
?>