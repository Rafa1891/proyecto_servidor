<?php
    
    session_start();

    if(!isset($_SESSION["posicion"])){
    $_SESSION["posicion"]=0;
}

function recoge($var){
 $tmp =(isset($_REQUEST[$var]))
 ? trim(htmlspecialchars($_REQUEST[$var],ENT_QUOTES,"UTF-8")):"";
 return $tmp;
}
$accion=recoge("boton");
$accionOk=false;

if($accion !="izda" && $accion !="dcha" && $accion !="centro"){
  header("Location:E2.php");
  exit;
}else{
  $accionOk=true;
}

if($accionOk){
  if($accion=="centro"){
    $_SESSION["posicion"]=150;
  }else if($accion =="izda"){
    $_SESSION["posicion"]-=20;
  }else if($accion =="dcha"){
    $_SESSION["posicion"]+=20;
  }

  if($_SESSION["posicion"]>300){
    $_SESSION["posicion"]=0;
  }else if($_SESSION["posicion"]<0){
    $_SESSION["posicion"]=300;
  }
  header("Location:E2.php");
  exit;
}

?>