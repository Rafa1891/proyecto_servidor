<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <title>Ejercicio 2</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <style type="text/css">
 body{
  margin-left:200px;
 }
 </style>
</head>
<body>
  <h1>MOVER UN PUNTO A DERECHA E IZQUIERDA</h1>
  <p>Haga clic en los botones para mover el punto:</p><br/>
  <form method="post" action="E2b.php">
  <?php 
  
  session_start();

  if(!isset($_SESSION["posicion"])){
    $_SESSION["posicion"]=0;
  }
   ?>
  
	<button name="boton" type="submit" value="izda">Izquierda</button>&nbsp
  <button  type="submit" name="boton" value="dcha">Derecha</button><br/>
  <svg >
  <line x1="-300" y1="10" x2="300" y2="10" stroke="black" stroke-width="5" />
  <circle cx="<?php echo $_SESSION['posicion']; ?>" cy="10" r="8" fill="red" />  
</svg><br/>
<button type="submit" name="boton" value="centro">Volver al centro</button>

</form>
</body>
</html>