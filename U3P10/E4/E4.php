<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <title>Ejercicio 2</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <style type="text/css">
 body{
  margin-left:200px;
 }
 #naranja{
  color:orange;
  line-height: 50px;
  font-size:60px;
 }
 #azul{
  color:blue;
  line-height: 50px;
  font-size:60px;
 }
 </style>
</head>
<body>
  <h1>VOTAR UNA OPCIÓN</h1>
  <p>Haga clic en los botones para votar por una opción:</p><br/>
  <form method="post" action="E4b.php">
  <?php 
  
  session_start();

      if(!isset($_SESSION["a"]) || !isset($_SESSION["b"])){
    $_SESSION["a"]=$_SESSION["b"]=0;
}
   ?>
  
	<button id="azul" name="boton" type="submit" value="a">✔</button>
  <svg width="<?php echo $_SESSION['a'];?>" height="50">
  <line x1="0" y1="25" x2="<?php echo $_SESSION['a'];?>" y2="25" stroke="blue" stroke-width="50" /> 
</svg><br/><br/>
  <button id="naranja" type="submit" name="boton" value="b">✔</button>
  <svg width="<?php echo $_SESSION['b'];?>" height="50">
  <line x1="0" y1="25" x2="<?php echo $_SESSION['b'];?>" y2="25" stroke="orange" stroke-width="50" /> 
</svg><br/><br/>
<button type="submit" name="boton" value="cero">Poner a cero</button>

</form>
</body>
</html>