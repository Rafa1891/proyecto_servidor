<?php
    
    session_start();

    if(!isset($_SESSION["a"]) || !isset($_SESSION["b"])){
    $_SESSION["a"]=$_SESSION["b"]=0;
}

function recoge($var){
 $tmp =(isset($_REQUEST[$var]))
 ? trim(htmlspecialchars($_REQUEST[$var],ENT_QUOTES,"UTF-8")):"";
 return $tmp;
}
$accion=recoge("boton");
$accionOk=false;

if($accion !="a" && $accion !="b" && $accion !="cero"){
  header("Location:E4.php");
  exit;
}else{
  $accionOk=true;
}

if($accionOk){
  if($accion=="cero"){
    $_SESSION["a"]=$_SESSION["b"]=0;
  }else if($accion =="a"){
    $_SESSION["a"]+=10;
  }else if($accion =="b"){
    $_SESSION["b"]+=10;
  }

  
  header("Location:E4.php");
  exit;
}
?>