<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <title>Ejercicio 1</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <style type="text/css">
 	button{
 		font-size: 34px;
 	}
 	strong{
 		font-size: 34px;
 	}
 	#botoncero{
 		font-size: 12px;
 	}
 </style>
</head>
<body>
	<h1>Subir y bajar número</h1><br/>
	<p>Haga clic en los botones para modificar el valor:</p>
	<form method="post" action="E1b.php">
    <button name="boton" type="submit" value="sumar">+</button>
    <?php
    session_start();
    if(!isset($_SESSION["numero"])){
    $_SESSION["numero"]=0;
}
echo "<strong>".$_SESSION["numero"]."</strong>";
?>
 
<button name="boton" type="submit" value="restar">-</button><br/><br/>
<button id="botoncero" name="boton" type="submit" value="cero">Poner a cero</button>
</form>
</body>
</html>