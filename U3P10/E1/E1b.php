<?php
    session_start();
    if(!isset($_SESSION["numero"])){
    $_SESSION["numero"]=0;
}

function recoge($var){
 $tmp =(isset($_REQUEST[$var]))
 ? trim(htmlspecialchars($_REQUEST[$var],ENT_QUOTES,"UTF-8")):"";
 return $tmp;
}
$accion=recoge("boton");
$accionOk=false;

if($accion !="sumar" && $accion !="restar" && $accion !="cero"){
  header("Location:E1.php");
  exit;
}else{
  $accionOk=true;
}

if($accionOk){
  if($accion=="cero"){
    $_SESSION["numero"]=0;
  }else if($accion =="sumar"){
    $_SESSION["numero"]++;
  }else if($accion =="restar"){
    $_SESSION["numero"]--;
  }
  header("Location:E1.php");
  exit;
}
?>
 
