<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <title>Ejercicio 20</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <style>
    h2{
      text-align: center;
    }
    form{      
      width:750px;
      margin-left: 150px;

    }
    legend{
      background-color: white;
      border:2px solid #D1C6F8;
    }
    fieldset{
      border:2px solid #D1C6F8;
      background-color: #E9DFFA;
    }
    #uno{
      margin-left: 200px;
      width:1000px;
      background-color: #E9ECE8;
      padding-bottom: 10px;
    }
    #dos{
      margin-left: 550px;
    }
    hr{
      border:2px solid #CED3CC;
    }
    strong{
      font-size: 20px;
    }
  </style>
</head>

<body>
  <div id="uno">
    <hr>    
<h2>ECUACIÓN DE SEGUNDO GRADO (FORMULARIO)</h2>
<?php
  
  if(isset($_POST['calcular'])==false && isset($_POST['borrar'])==false) {
  
    ?>
<form action="" method="post">
  <fieldset>
  <legend>Formulario</legend>
    Dada la ecuación de segundo grado <strong>a*x^2+b*x+c=0</strong>, escriba los valores de los tres coeficientes y resolveré la ecuación:<br/><br/>
        
        a:<input type="text" name="a" ><br/><br/>
        b:<input type="text" name="b" ><br/><br/>
        c:<input type="text" name="c" >
     <br>
    <br/>
    <div id="dos">
    <input type="submit" name="calcular" value="Calcular">
    <input type="submit" name="borrar" value="Borrar">
  </div>
    </fieldset>
</form>

<?php
}else{
if(isset($_POST['calcular'])){
  $a=$_POST['a'];
  $b=$_POST['b'];
  $c=$_POST['c'];

  if((pow($b,2)-(4*$a*$c))<0){
  echo "No hay solución";

  }else if((pow($b,2)-(4*$a*$c))>0){

   $x=(-$b+sqrt(pow($b,2)-4*$a*$c))/(2*$a);
   $x2=(-$b-sqrt(pow($b,2)-4*$a*$c))/(2*$a);

  echo "El valor de x es: ".$x." y : ".$x2;
}else{
   $x=-$b/(2*$a);

   echo "El valor de x es: ".$x;
}
 
}elseif(isset($_POST['borrar'])){
  ?>
  <form action="" method="post">
  <fieldset>
  <legend>Formulario</legend>
    Dada la ecuación de segundo grado <strong>a*x<sup>2</sup>+b*x+c=0</strong>, escriba los valores de los tres coeficientes y resolveré la ecuación:<br/><br/>
        
        a:<input type="text" name="a" ><br/><br/>
        b:<input type="text" name="b" ><br/><br/>
        c:<input type="text" name="c" >
     <br>
    <br/>
    <div id="dos">
    <input type="submit" name="calcular" value="Calcular">
    <input type="submit" name="borrar" value="Borrar">
  </div>
    </fieldset>
</form> 
<?php 
   }
 }
?>

</div>
</body>
</html>