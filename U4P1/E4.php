<?php 

class Persona{
	
	protected $nombre;
	protected $edad;

	public function leerPersona($n,$e){
	$this->nombre=$n;
	$this->edad=$e;
	}

	public function imprimirPersona(){
	echo "Nombre: ".$this->nombre." Edad: ".$this->edad;
	}

	}
class Empleado extends Persona{
	protected $sueldo;
    
    public function leerEmpleado($s){
    	$this->sueldo=$s;
    }

    public function imprimirEmpleado(){
    	
    	echo '.Sueldo: '.$this->sueldo;
    }
}

$p1=new Persona();
$p1->leerPersona('jose',22);
$p1->imprimirPersona();
$p2=new Empleado();
$p2->leerPersona('ana',23);
$p2->leerEmpleado(2000);
$p2->imprimirPersona();
$p2->imprimirEmpleado();



	
?>