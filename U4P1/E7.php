<!DOCTYPE html>
<html>
<head>
<title>Ejercicio 7</title>
</head>
<body>
<?php

class Opcion {
  private $titulo;
  private $enlace;
  private $colorf;
  private $url;

  public function __construct($url){
    $this->url=$url;
  }

  public function leerOpcion($tit,$enl,$cf){
    $this->titulo=$tit;
    $this->enlace=$enl;  
    $this->colorf=$cf;
  }
  
  public function graficar(){
    ?>
    <form action="" method="post">Título de la web a mostrar:
    <input type="text" name=<?php echo "nombre".$this->url?>> 
    URL <?php echo $this->url?> a mostrar:<input type="text" name=<?php echo "link".$this->url?>>
     Color: <input type="color" name=<?php echo "color".$this->url?>></br>
     
     <?php
     
  }
  public function graficarOpciones(){
    echo '<a style="background-color:'.$this->colorf.
         '" href="'.$this->enlace.'">'.$this->titulo.'</a>';
  }

}

class Menu {
  private $opciones=array();

  public function insertar($op){
    $this->opciones[]=$op;  
  }

  public function graficarHorizontal(){
    for($f=0;$f<count($this->opciones);$f++)
    {
      $this->opciones[$f]->graficarOpciones();
    }
  }

  public function graficarVertical(){
    for($f=0;$f<count($this->opciones);$f++)
    {
      $this->opciones[$f]->graficarOpciones();
      echo '<br>';
    }
  }
    public function graficarOrientacion(){
      echo "Orientación:</br>
      <input type='radio' name='orientacion' value='horizontal'>Horizontal</br> 
      <input type='radio' name='orientacion' value='vertical'>Vertical</br>
      <input type='submit' name='generar' value='Generar'>
      </form>";
    }
   
  
}
$opcion[0]=new Opcion(1);
$opcion[1]=new Opcion(2);	
$opcion[2]=new Opcion(3);
    for($i=0;$i<3;$i++){
      $opcion[$i]->graficar();
    }
    $menu=new Menu();
    $menu->graficarOrientacion();
    
    
    if(isset($_POST["generar"])){
      $nombre1=$_POST["nombre1"];
      $nombre2=$_POST["nombre2"];
      $nombre3=$_POST["nombre3"];
      $link1=$_POST["link1"];
      $link2=$_POST["link2"];
      $link3=$_POST["link3"];
      $color1=$_POST["color1"];
      $color2=$_POST["color2"];
      $color3=$_POST["color3"];
     
      
		$opcion[0]->leerOpcion($nombre1,$link1,$color1);
		$menu->insertar($opcion[0]);
		$opcion[1]->leerOpcion($nombre2,$link2,$color2);
		$menu->insertar($opcion[1]);
		$opcion[2]->leerOpcion($nombre3,$link3,$color3);
    $menu->insertar($opcion[2]);
      
      if ($_POST["orientacion"]=="horizontal")
      $menu->graficarHorizontal();
    else
      if ($_POST["orientacion"]=="vertical")
        $menu->graficarVertical();
  }
    
?>
</body>
</html>