<?php 

class Empleado{
	protected $nombre;
	protected $sueldo;

	public function leerDatos($n,$s){
	$this->nombre=$n;
	$this->sueldo=$s;
    }
    
	public function imprimirDatos(){
	echo "Nombre: ".$this->nombre." Sueldo: ".$this->sueldo;
    if($this->sueldo>3000){
        echo " .Debe pagar impuestos.";
    }
}

    }

    $persona1=new Empleado();
    $persona1->leerDatos('ANDRÉS',1000);
    $persona1->imprimirDatos();
    $persona2=new Empleado();
    $persona2->leerDatos('RAFA',5000);
    $persona2->imprimirDatos();
	
?>