<?php 

class Empleado{
	protected $nombre;
	protected $sueldo;

	public function __construct($n,$s){
	$this->nombre=$n;
	$this->sueldo=$s;
    }
    public function __destruct(){
	echo "La clase ".__CLASS__." fue destruída.";
    }
	public function imprimirDatos(){
	echo "Nombre: ".$this->nombre." Sueldo: ".$this->sueldo;
    if($this->sueldo>3000){
        echo " .Debe pagar impuestos.";
    }
}

    }

    $persona1=new Empleado('ANDRÉS',1000);    
    $persona1->imprimirDatos();
    unset($persona1);
    $persona1->__destruct();
    $persona1->imprimirDatos();
    $persona2=new Empleado('RAFA',5000);
    $persona2->imprimirDatos();
	
?>