<?php 

class Libro{
	
	protected $titulo;
	protected $autor;
    protected $anio;
    protected $hojas;
    protected $editorial;

	public function __construct($tit,$aut,$an,$hoj,$edi){
	$this->titulo=$tit;
	$this->autor=$aut;
    $this->anio=$an;
    $this->hojas=$hoj;
    $this->editorial=$edi;
	}

	public function __toString(){
    echo "<h2>Titulo: ".$this->titulo."</h2>
    <h3>Autor: ".$this->autor."</h3>
    <h3>Año de publicación: ".$this->anio."</h3>
    <h3>Hojas: ".$this->hojas."</h3>
    <h3>Editorial: ".$this->editorial."</h3>";
	}
}
	

$l1=new Libro('Los pilares de la tierra','Ken Follett',2001,1040,'Plaza & Janes Editores');
$l1->__toString();
	
?>