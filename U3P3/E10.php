<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <title>Ejercicio 10</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>  
	
	<?php
if (empty($_POST["name"])) {
$nameErr = "Name is required";
} else {
$name = test_input($_POST["name"]);
// check if name only contains letters and whitespace
if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
$nameErr = "Only letters and white space allowed";
}
}
//Si el valor de name está vacío muestra "Name is required", si no comprueba que solo contenga letras y espacio, con preg_match si encuentra alguno de los caracteres introducidos en el parámetro para la variable name muestra el mensaje de error "Only letters and white space allowed".
 ?>
</body>
</html>