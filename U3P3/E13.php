<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <title>Ejercicio 13</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <style>
  	.error{
  		color:red;
  	}
  </style>
</head>
<body>  
	<h1>PHP Form Validation Example</h1>
	<?php
$nombre="";
$email="";
$url="";
$gender="";
$nombreErr = "";
$emailErr = "";
$urlErr = "";
$genderErr="";


if(isset($_POST["submit"])){
	
$nombre=$_POST["nombre"];
$email=$_POST["email"];
$url=$_POST["url"];
$gender=$_POST["gender"];

if (empty($_POST["nombre"])) {
$nombreErr = "Name is required";
} else {
if (!preg_match("/^[a-zA-Z ]*$/",$nombre)) {
$nombreErr = "Only letters and white space allowed";
}
}

if(empty($email)){
$emailErr = "E-mail is required.";
} else {
if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
$emailErr = "Invalid E-mail.";
}
}

if(empty($url)){
$urlErr = "URL is required.";
} else {
if (!filter_var($url, FILTER_VALIDATE_URL)) {
$urlErr = "Invalid URL.";
}
}
if(!isset($gender)){
	$genderErr="Gender is required.";
}


	
}
?>
		<form method="post" action="E13.php">
			Name: <input type="text" name="nombre" value=""><span class="error">* <?php  echo $nombreErr;?></span><br/><br/>
			E-mail: <input type="email" name="email" value=""><span class="error">* <?php echo $emailErr;?></span><br/><br/>
			Website: <input type="url" name="url" value=""><span class="error">* <?php echo $urlErr;?></span><br/><br/>
			Comment: <textarea rows="5" cols="20"></textarea><br/><br/>
			Gender:<input type="radio" name="gender" value="female">Female &nbsp<input type="radio" name="gender"   value="male">Male<span class="error">* <?php echo $genderErr;?></span><br/><br/>
	<input type="submit" name="submit">
</form>

<br/>
<h2>Your Input:</h2>
<br/>

<?php

echo $nombre;
echo "<br>";
echo $email;
echo "<br>";
echo $url;
echo "<br>";
if (isset($gender) && $gender=="female"){
		echo $gender;
	}
	if (isset($gender) && $gender=="male"){
		echo $gender;
	}



?>
</body>
</html>