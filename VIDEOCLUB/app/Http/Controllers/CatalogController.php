<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illumninate\support\Facades\Redirect;
use App\Movie;

class CatalogController extends Controller
{
    

    public function getIndex(){
      $peliculas=Movie::all();
     return view("catalog.index",array('arrayPeliculas'=>$peliculas));
    }
    public function getShow($id){
        $peliculas=Movie::find($id);
     return view("catalog.show",array('pelicula'=>$peliculas));
    }
    public function getCreate(){
      return view("catalog.create");
    }
    public function postCreate(Request $request){
    $pelicula=new Movie();
    $pelicula->title=$request->input('title');
    $pelicula->year=$request->input('year');
    $pelicula->director=$request->input('director');
    $pelicula->poster=$request->input('poster');
    $pelicula->synopsis=$request->input('synopsis');
    $pelicula->save();
    return redirect('catalog');
    }

    public function getEdit($id){
        $pelicula=Movie::find($id);       
        return view("catalog.edit",array('pelicula'=>$pelicula));
    }

    public function putEdit(Request $request, $id){
        $pelicula=Movie::find($id);
        $pelicula->title=$request->input('title');
    $pelicula->year=$request->input('year');
    $pelicula->director=$request->input('director');
    $pelicula->poster=$request->input('poster');
    $pelicula->synopsis=$request->input('synopsis');
    $pelicula->save();
    return redirect('catalog');

    }
    public function getDelete($id){
        $pelicula=Movie::find($id);
        return view("catalog.delete",array('pelicula'=>$pelicula));
    }
    public function putDelete( $id){
        $pelicula=Movie::find($id);
        $pelicula->delete();
    return redirect('catalog');

    }

}
