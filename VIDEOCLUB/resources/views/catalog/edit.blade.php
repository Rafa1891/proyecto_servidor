@extends('layouts.master')

@section('content')
<h1>Editar película</h1>
<form action="{{action('CatalogController@putEdit',$pelicula->id)}}" method="POST">
<input type="hidden" name="_method" value="PUT">
<input type="hidden" name="_token" value="{{csrf_token()}}">
Title:<input type="text" name="title" value="<?php echo $pelicula->title ?>"></br>
Año:<input type="text" name="year" value="<?php echo $pelicula->year ?>"></br>
Director:<input type="text" name="director" value="<?php echo $pelicula->director ?>"></br>
Poster:<input type="text" name="poster" value="<?php echo $pelicula->poster ?>"></br>
Sinopsis:<textarea name="synopsis" value="<?php echo $pelicula->synopsis ?>"></textarea></br>
<input type="submit" value="Enviar"></br>
</form>
@stop