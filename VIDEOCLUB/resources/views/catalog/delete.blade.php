@extends('layouts.master')

@section('content')
<h1>Eliminar película</h1>
<form action="{{action('CatalogController@putDelete',$pelicula->id)}}" method="POST">
<input type="hidden" name="_method" value="DELETE">
<input type="hidden" name="_token" value="{{csrf_token()}}">
<?php
echo "<h1>Película</h1>";
echo "Título: ".$pelicula->title;
echo "</br>";
echo "Año: ".$pelicula->year;
echo "</br>";
echo "Director: ".$pelicula->director;
echo "</br>";
echo "Poster: ".$pelicula->poster;
echo "</br>";
echo "Alquilada: ".$pelicula->rented;
echo "</br>";
echo "Sinopsis: ".$pelicula->synopsis;
echo "</br>";
?>
<input type="submit" value="Eliminar"></br>
</form>
@stop