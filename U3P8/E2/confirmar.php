<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <title>Confirmar compra</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <style type="text/css">
   body{
    background-color: #FFE7DE;
    
   }
   h2{
    color:#588CB5;
   }
   div{
    padding: 5px;
    border:1px solid #A2E868;
   	color:#588CB5;
    margin-left: 400px;
    margin-top: 100px;
    width: 410px;
    background-color: white;
   }
   a{
    text-decoration: none;
    color:#588CB5;
    
   }
   a:hover{
    color:#A2E868;
   }
 </style>
</head>
<body>
  <div>
	<h2>Felicidades acaba de adquirir:</h2><br/>
	
	<?php
	session_start();
     
	   echo $_SESSION['carrito']['tele']." teles.<br/>";
     echo $_SESSION['carrito']['movil']." moviles.<br/>";
     echo $_SESSION['carrito']['mp4']." mp4.<br/>";
     echo $_SESSION['carrito']['raton']." ratones.<br/>";
     echo $_SESSION['carrito']['alfombrilla']." alfombrillas.<br/>";
     echo $_SESSION['carrito']['usb']." usb.<br/>";
     echo "Total: ".$_SESSION['total']." € .";
	?>
  <h3>Gracias por su compra</h3>
  <a href="formulario.php" onclick="<?php session_destroy();?>">Terminar</a>
</div>
</body>
</html>