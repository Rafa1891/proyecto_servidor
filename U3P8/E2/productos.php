<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <title>Productos</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <style type="text/css">
   body{
    background-color: #FFE7DE;
    padding-left: 25px;
   }
   h1{
    color:#588CB5;
   }
   div{
    padding: 5px;
    border:1px solid #A2E868;
    float:left;
    margin-right: 10px;
    color:#588CB5;
    width: 180px;
    height:350px;
    background-color: white;
   }
   #compra{
     padding: 5px;
    border:1px solid #A2E868;
    margin-left: 500px;
    margin-top: 20px;
    margin-right: 20px;
    margin-bottom: 10px;
    color:#588CB5;
    height:520px;
   }
   ul{
    list-style: none;
    margin-left: -20px;
    margin-right: 10px;
   }
   h2{
    color:#588CB5;
    font-size: 24px;
    margin-left: 500px;
   }
   strong{
    color:#588CB5;
   }
   button{
    font-size: 20px;
    color:white;
    background-color: #588CB5;
    margin-left: 10px;

   }
   img{
    margin-left: 15px;
   }
   #botonconfirmar{
    margin: auto;
   }
  #botonaniadir{
   position: absolute;
   bottom: 450px;
  }
 </style>
</head>
<body>
  <?php



    session_start();

    if(isset($_POST["ingresar"])){

      if(empty($_POST["nombre"])){
  header("Location:formulario.php");
}
      
      $carrito=array('tele'=>0,'movil'=>0,'mp4'=>0,'raton'=>0,'alfombrilla'=>0,'usb'=>0);
      $_SESSION['carrito']=$carrito;
      $_SESSION["nombre"]=$_POST["nombre"];  
      }
           
    
      
    ?>
    <h2>Bienvenid@ <?php echo $_SESSION["nombre"];
    
    $tele=array('nombre'=>'tele','precio'=>210);
   $movil=array('nombre'=>'movil','precio'=>300);
   $mp4=array('nombre'=>'mp4','precio'=>13);
   $raton=array('nombre'=>'raton','precio'=>20);
   $alfombrilla=array('nombre'=>'alfombrilla','precio'=>30);
   $usb=array('nombre'=>'usb','precio'=>5);
    ?></h2><br/><br/>
    <form method="post" action="">
     <div>
      <img src="tele.jpg" alt="" width="150" height="150"><br/>
    <ul>
      <li>Producto:<strong>Television 4K</strong></li>
      <li>Descripción:<strong>22 pulgadas</strong></li>
      <li>Precio:<strong>210 €</strong></li>
    </ul><br/>
    <button id="botonaniadir" name="aniadir" value="<?php echo $tele['nombre'];?>"  type="submit">Añadir al carrito</button>
  </div>
  <div>
    <img src="movil.jpg" alt="" width="150" height="150"><br/>
    <ul>
      <li>Producto:<strong>Móvil</strong></li>
      <li>Descripción:<strong>4g</strong></li>
      <li>Precio:<strong>300 €</strong></li>
    </ul>
    <br/>
    <button id="botonaniadir" name="aniadir" value="<?php echo $movil['nombre'];?>" type="submit" >Añadir al carrito</button>
  </div>
  <div>
    <img src="mp4.jpg" alt="" width="150" height="150"><br/>
    <ul>
      <li>Producto:<strong>mp4</strong></li>
      <li>Descripción:<strong>20 Gb</strong></li>
      <li>Precio:<strong>13 €</strong></li>
    </ul>
    <br/>
    <button id="botonaniadir" name="aniadir" value="<?php echo $mp4['nombre'];?>" type="submit">Añadir al carrito</button>
  </div>
  <div>
    <img src="raton.jpg" alt="" width="150" height="150"><br/>
    <ul>
      <li>Producto:<strong>Ratón</strong></li>
      <li>Descripción:<strong>6000 dpi</strong></li><strong></strong>
      <li>Precio:<strong>20 €</strong></li>
    </ul>
    <br/>
    <button id="botonaniadir" name="aniadir" value="<?php echo $raton['nombre'];?>" type="submit">Añadir al carrito</button>
  </div>
  <div>
    <img src="alfombrilla.jpg" alt="" width="150" height="150"><br/>
    <ul>
      <li>Producto:<strong>Alfombrilla</strong></li>
      <li>Descripción:<strong>Negra</strong></li>
      <li>Precio:<strong>30 €</strong></li>
    </ul>
    <br/>
    <button id="botonaniadir" name="aniadir" value="<?php echo $alfombrilla['nombre'];?>" type="submit">Añadir al carrito</button>
  </div>
  <div>
    <img src="usb.jpg" alt="" width="150" height="150"><br/>
    <ul>
      <li>Producto:<strong>Usb</strong></li>
      <li>Descripción:<strong>2 Gb</strong></li>
      <li>Precio:<strong>5 €</strong></li>
    </ul>
    <br/>
    <button id="botonaniadir" name="aniadir" value="<?php echo $usb['nombre'];?>" type="submit">Añadir al carrito</button>
  </div>
  <br/>
  <div id="compra">
  <h1>Su compra:</h1>
  <br/>
  
  <?php
 
   if(isset($_POST["aniadir"])){ 
    
    $_SESSION['carrito'][$_POST["aniadir"]]+=1;
    
    }
    if(isset($_POST["eliminar"])){
      if($_SESSION['carrito'][$_POST["eliminar"]]>=1){
    $_SESSION['carrito'][$_POST["eliminar"]]-=1;
  }

  }
  if(isset($_POST["aniadir"]) || isset($_POST["eliminar"])){
    $numTeles=$_SESSION['carrito']['tele'];
   $numMoviles=$_SESSION['carrito']['movil'];
   $numMp4=$_SESSION['carrito']['mp4'];
   $numRatones=$_SESSION['carrito']['raton'];
   $numAlfombrillas=$_SESSION['carrito']['alfombrilla'];
   $numUsbs=$_SESSION['carrito']['usb'];
   
  $total=($numTeles*$tele['precio'])+($numUsbs*$movil['precio'])+($numMp4*$mp4['precio'])+($numRatones*$raton['precio'])+($numAlfombrillas*$alfombrilla['precio'])+($numUsbs*$usb['precio']);
  
  $_SESSION['total']=$total;
  }
  

  
    echo $_SESSION['carrito']['tele']." x ".$tele['nombre']." ".$tele['precio']." € ." ?>
    <button name='eliminar' value='<?php echo $tele['nombre'];?>' >Eliminar</button><br/><br/>

    <?php echo $_SESSION['carrito']['movil']." x ".$movil['nombre']." ".$movil['precio']." € ."?>
    <button name='eliminar' value='<?php echo $movil['nombre'];?>' >Eliminar</button><br/><br/>

    <?php echo $_SESSION['carrito']['mp4']." x ".$mp4['nombre']." ".$mp4['precio']." € ."?>
    <button name='eliminar' value='<?php echo $mp4['nombre'];?>' >Eliminar</button><br/><br/>

    <?php echo $_SESSION['carrito']['raton']." x ".$raton['nombre']." ".$raton['precio']." € ."?>
    <button name='eliminar' value='<?php echo $raton['nombre'];?>' >Eliminar</button><br/><br/>

    <?php echo $_SESSION['carrito']['alfombrilla']." x ".$alfombrilla['nombre']." ".$alfombrilla['precio']." € ."?>
    <button name='eliminar' value='<?php echo $alfombrilla['nombre'];?>' >Eliminar</button><br/><br/>

    <?php echo $_SESSION['carrito']['usb']." x ".$usb['nombre']." ".$usb['precio']." € ."?>
    <button name='eliminar' value='<?php echo $usb['nombre'];?>' >Eliminar</button><br/>
    
  </form>

<br/>
<form method="post" action="confirmar.php">
<button name="confirmar" id="botonconfirmar">Confirmar compra</button>
</form>
</div>
</body>
</html>